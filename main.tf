terraform {
  required_version = ">= 0.13.2"
}

resource "aws_ecs_task_definition" "ecs_task_definition" {
  family                    = var.name.slug
  container_definitions     = var.definitions

  dynamic "volume" {
    for_each = var.volume != null ? [1] : []

    content {
      name = var.volume.name

      docker_volume_configuration {
        scope         = "shared"
        autoprovision = true
        driver        = var.volume.driver // "rexray/efs"
      }
    }
  }

  requires_compatibilities  = var.compatibilities
  cpu                       = var.cpu
  memory                    = var.memory
  network_mode              = var.network_mode
  execution_role_arn        = var.execution_role_arn
  task_role_arn             = var.execution_role_arn

  tags = {
    Name                    = var.name.full
    Environment             = var.environment
  }
}
