# Task Definition Module

This module is capable to generate a task description for ECS usage

Module Input Variables
----------------------

- `environment` - environment name
- `name` - general name
- `execution_role_arn` - task execution role to be used
- `definitions` - task description (json format)
- `volume` - volume to attach, will be created with rexray/efs or rexray/s3fs docker plugin
- `compatibilities` - EC2 or FARGATE compatibility (default ["FARGATE"])
- `cpu` - CPU units to dedicate (default 256)
- `memory` - Amount of memory to dedicate (default 512)
- `network_mode` - Network mode to use (default awsvpc)

Usage
-----

```hcl
module "task" {
  source              = "git::https://gitlab.com/mesabg-tfmodules/task.git"

  environment         = "environment"
  name = {
    slug              = "slugname"
    full              = "Full Name"
  }

  execution_role_arn  = "arn:aws:iam::someExecutionRole"
  definitions         = <<EOF
  [
    {
      "name":"task-sample",
      "command":[
        "somecommand"
      ],
      "cpu":256,
      "entryPoint":[
        "/bin/sh",
        "-c"
      ],
      "environment":[

      ],
      "essential":true,
      "image":"nginx:latest",
      "memory":512,
      "memoryReservation":512,
      "mountPoints":[
        
      ],
      "portMappings":[
        
      ],
      "volumesFrom":[
        
      ],
      "workingDirectory":"/app"
    }
  ]
  EOF

  volume = {
    name              = "volume-name"
    driver            = "rexray/efs"
  }

  compatibilities     = ["EC2", "FARGATE"]
  cpu                 = 256
  memory              = 512
  network_mode        = "awsvpc"
}
```

```hcl
module "task" {
  source              = "../../modules/task"

  environment         = "environment"
  name = {
    slug              = "slugname"
    full              = "Full Name"
  }

  execution_role_arn  = "arn:aws:iam::someExecutionRole"

  definitions         = <<EOF
  [
    {
      "name":"task-sample",
      "command":[
        "somecommand"
      ],
      "cpu":256,
      "entryPoint":[
        "/bin/sh",
        "-c"
      ],
      "environment":[

      ],
      "essential":true,
      "image":"nginx:latest",
      "memory":512,
      "memoryReservation":512,
      "mountPoints":[
        
      ],
      "portMappings":[
        
      ],
      "volumesFrom":[
        
      ],
      "workingDirectory":"/app"
    }
  ]
  EOF
}
```

Outputs
=======

 - `task_definition_arn` - Created Task Definition resource identifier


Authors
=======
##### Moisés Berenguer <moises.berenguer@gmail.com>
