variable "environment" {
  type        = string
  description = "Environment name"
}

variable "name" {
  type = object({
    slug      = string
    full      = string
  })
  description = "General task name"
}

variable "execution_role_arn" {
  type        = string
  description = "Execution role dedicated"
}

variable "definitions" {
  type        = string
  description = "Task definition"
}

variable "volume" {
  type = object({
    name    = string
    driver  = string
  })
  description         = "Volume to attach to the task"
  default             = null
}

variable "compatibilities" {
  type        = list(string)
  description = "Compatibility Types"
  default     = ["FARGATE"]
}

variable "cpu" {
  type        = number
  description = "CPU units size"
  default     = 256
}

variable "memory" {
  type        = number
  description = "Amount of memory dedicated"
  default     = 512
}

variable "network_mode" {
  type        = string
  description = "Network Mode Type"
  default     = "awsvpc"
}
